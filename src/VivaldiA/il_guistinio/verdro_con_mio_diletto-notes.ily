\version "2.18.2"
#(ly:set-option 'relative-includes #t)


\include "italiano.ly"
\include "../../../includes/paper.ily"
\include "../../../includes/choir.ily"
\include "../../../includes/instruments.ily"
\include "../composer.ily"

\header {
    title = "Act I, Sc.8. Aria of Anastasio: Vedrò con mio diletto"
    subtitle = "Il Giustino (RV 717)"
}

#(set-global-staff-size 18)

Tempo = \markup{\small{\bold{"Larghetto"}}}

Common = {
  \key re \major
  \time 3/4
  \mark \Tempo
}

PianoRight = {
  r8\p <fad' re'> <fad' re'> <re'' fad'> <re'' fad'> <sol' re'> |
  <dod' mi' sol'> <dod' fad'> <dod' fad'> <fad' dod''> <fad' dod''> <dod' fad'> |
  <si re' fad'> <si mi'> <si mi'> <mi' si'> <mi' si'> <si mi'> |
  <lad dod' mi'>4 r8 <mi' dod''> <mi' dod''> <dod' mi'> |
  <si mi'> <si re'> <si re'> <re' si'> <re' si'> <re' fad'> |

  <dod' mi'> <mi' dod''> re' <re' si'> <lad dod'> <dod' lad'> |
  <re' si'>4 r4 r |
  R2. |
  r8\p <fad' re'> <fad' re'> <re'' fad'> <re'' fad'> <sol' re'> |
  <dod' mi' sol'> <dod' fad'> <dod' fad'> <fad' dod''> <fad' dod''> <dod' fad'> |

  <si re' fad'> <si mi'> <si mi'> <mi' si'> <mi' si'> <si mi'> |
  mi' dod'' re' si' dod' lad' |
  <re' si'>\pp <re' fad'> <re' fad'> <fad' re''> <fad' re''> <re' sol'> |
  <dod' mi' sol'> <dod' fad'> <dod' fad'> <fad' dod''> <fad' dod''> <dod' fad'> |

  <si re' fad'> <si mi'> <si mi'> <mi' si'> <mi' si'> <si mi'> |
  <la dod' mi'> <la re'> <la re'> <re' la'> <re' la'> <la re'> |
  <sol si re'> <sol dod'> <sol dod'> <dod' la'> <dod' la'> <mi' dod''> |
  <re' la'> <re' fad'> <re' fad'> <la' re''>\cresc <la' re''> <do'! la'> |
  <re' si'> <si re' sol'> <si re' sol'> <mi' si' mi''> <mi' si' mi''> <sol' si'> |

  <mi' dod''> <dod' la'> <dod' la'> <la' dod'' mi''> <la' dod'' mi''> <mi' la' dod''> |
  <re' la' re''> <re' la'> <re' la'> <la' fad''> <la' fad''> <la' re''> |
  <mi' si'> <mi' si'> <dod' la'> <dod' la'> <dod' la'> <dod' mi'> |
  <la fad'>\mf <fad la re'> <fad la re'> <la fad'> <la fad'> <re' fad' la'> |
  <re' fad' la'> <fad' la' re''> <fad' la' re''> <la' fad''> <la' fad''> <fad' la'> |

  <si fad'>\p <si fad'> <fad' si'> <fad' do''!> <fad' si'> <si fad' la'> |
  <si mi' sol'> <fad' si'> <mi' sol' si'> <si' sol''> <si' sol''> <sol' do''> |
  <fad' la' do''> <fad' si'> <fad' si'> <si' fad''> <si' fad''> <fad' si'> |
  <mi' sol' si'> <mi' la'> <mi' la'> <la' mi''> <la' mi''> <mi' la'>( |
  <mi' fad' la'>) <red' fad'> <red' fad'> <fad' la'> <fad' la'> <fad' la' re''> |

  <sol' si'> <sol' si' mi''> <sol' dod'' mi''> <dod'' mi'' sol''> <dod'' mi'' sol''> <sol' dod'' mi''> |
  << { <dod'' mi''> <si' re''> <fad' si' re''> <fad' re'' fad''> <fad' re'' fad''> <re' fad' si'> } \\ {sol'4 s2} >> |
  <re' fad' si'>8 <re' fad'> <re' fad'> <re' fad' si'> <re' sol' si'> <re' sol' re''> |
  <mi' sol' dod''> <dod' mi' sol'> <dod' mi' sol'> <dod' mi' sol' dod''> <dod' mi' sol' dod''> <dod' fad' la'> |
  <re' fad' si'> <si re' fad'> <si re' fad'> <si re' fad' si'> <si re' fad' si'> <si mi' sol'> |

  <dod' mi' lad'> <lad dod' mi'> <lad dod' mi'> <lad dod' mi' lad'> <lad dod' mi' lad'> <lad dod' mi' fad'> |
  <si re' fad'> <si re'> <si re'> <re' fad' si'>\cresc <re' fad' si'> <re' fad' si' re''> |
  <mi' sol' si'> <si mi' sol'> <si mi' sol'> <mi' sol' si'> <mi' sol' si'> <sol' si' mi''> |
  <fad' lad' mi''> <lad dod' fad'> <lad dod' fad'> <fad' lad' dod''> <fad' lad' dod''> <dod' mi' fad' lad'> |
  <re' fad' si'> <si re' fad'> <lad dod' fad'> <dod' fad' si'> <dod' fad' si'> <dod' fad' lad'> |

  <re' fad' si'> <re' fad'> <re' fad'> <fad' re''> <fad' re''> <re' sol'> |
  <dod' mi' sol'> <dod' fad'> <dod' fad'> <fad' dod''>\dim <fad' dod''> <dod' fad'> |
  <si re' fad'> <si mi'> <si mi'> <mi' si'> <mi' si'> <si mi'> |
  <dod' mi'> <lad' dod''> <si re'> <re' si'> <lad dod'> <dod' lad'>\p |
  <re' si'>4\fermata r r8 <re' si'>\mf |

  sold' dod'' dod'' <re'' mid''> <dod'' mid''> <si' dod''> |
  <la' dod''> <sold' si'> <fad' la'> dod'' re'' re'' |
  si'8 sold' sold' dod'' dod'' sold' |
  la'8 fad' fad' si' si' fad' |

  sold'8 mid' mid' dod'' dod'' <mid' sold'> |
  la'\p fad' fad' <sold' dod''> <fad' dod''> <fad' re''> |
  si'8 sold' sold' dod'' dod'' si' |
  la'8 fad' fad' si' si' fad' |

  sold'8 mid' mid' dod'' dod'' <mid' sold'> |
  fad' la' la' dod'' dod'' la' |

  << {fad'8 fad' sold' dod'' dod'' mid'} \\ { s8 fad'4~ fad'8[ mid' dod'] } >> |
  <fad' dod'>2.\fermata |

  \bar "|."
}

PianoLeftUpper = {
  si,8 si si si si si |
  lad,8 lad la, la la la |
  sold,8 sold sol, sol sol sol
  <fad, fad>4 r8 s4. |
  fad8 fad fad fad fad si |

  lad8 lad si si fad fad |
  s4 \oneVoice r4 r |
  R2. \voiceOne |
  si,8 si si si si \change Staff = "RH" \stemDown re' \change Staff = "LH" \stemUp |
  s2. |

  s2. |
  <dod' lad>4( si la) |
  si,8 si si si si si |
  lad,8 lad la, la la la |

  sold,8 sold sol, sol sol sol |
  fad,8 fad fad fad fad fad |
  mi,8 mi la, sol sol sol |
  fad8 la la fad fad fad |
  sol8 sol sol sol sol, mi |

  la,8 mi mi mi sol, mi |
  fad,8 fad fad re re fad |
  sol,8 sol la, mi mi la |
  re,8 re re re re re |
  re8 re re re re re |

  red,8 red red red red red |
  mi,8 mi mi mi mi mi |
  red,8 red re, re re re |
  dod,8 dod do, do do do |
  si,,8 si, si, si, si, si, |

  mi,8 mi lad,, lad, lad, lad, |
  si,,8 si, si, si, si, si, |
  si,8 si si si si, si |
  lad,8 lad la, la la la |
  sold,8 sold sol, sol sol sol |

  fad,8 fad fad fad fad fad |
  si,,8 si, si, si, si, si, |
  mi,8 mi mi mi mi mi |
  fad,8 fad fad fad fad fad |
  si,4 s fad, |

  si,8 si si si si \change Staff = "RH" \stemDown si \change Staff = "LH" \stemUp |
  s2. |
  s2. |
  s2  fad4 |
  s4\fermata r r8 s |

  s2. |
  s2. |
  re'4 dod'2~ |
  dod'4 si2 |

  mid4 sol2 |
  s2. |
  re'4. dod'4. ~ |
  dod'4. si4. |

  mid4 sol2 |
  s2. |
  s2. |
  s2. \fermata|

  \bar "|."
}

PianoLeftLower = {
  si,2. |
  lad,4 la,2 |
  sold,4 sol,2 |
  s4 s8 <fad lad> <fad lad> <fad lad> |
  si,2 si,4 |

  dod4 fad fad, |
  <fad si,>4  s4 s |
  s2. |
  si,2 si4( |
  lad4 la4.) la8( |

  sold4 sol!4.) sol8( |
  fad2.) |
  si,2. |
  lad,4 la,2 |

  sold,4 sol,2 |
  fad,2. |
  mi,4 la,2 |
  re2 re4 |
  sol,2 sol,4 |

  la,2 sol,4 |
  fad,2. |
  sol,4 la,2 |
  re,2.~ |
  re,2. |

  re,2. |
  mi,2. |
  red,4 re,2 |
  dod,4 do,2 |
  si,,2. |

  mi,4 lad,,2 |
  si,,2. |
  si,2 si,4 |
  lad,4 la,2 |
  sold,4 sol,2 |

  fad,2. |
  si,,2. |
  mi,2. |
  fad,2. |
  s4 fad s |

  si,2 si4( |
  lad4 la2) |
  sold4( sol2) |
  <fad lad>4 <si, fad> fad8 fad, |
  <si, fad>4 r r8 si |

  <mid sold dod'>2. |
  <fad la dod'>2 <fad la>4 |
  <mid sold>4( <mi sold>2) |
  <red fad>4( <re fad>2) |

  <dod si>2. |
  <fad la dod'>2 <fad la>4 |
  <mid sold>4( <mi sold>2) |
  <red fad>4( <re fad>2) |

  <dod si>2. |
  <fad la dod'>2.~ |
  <fad la dod'>4 <dod sold dod'>2 |
  <fad la>2.|

  \bar "|."
}

SopranoVoice = \relative do' {
  \dynamicUp
  R2.*6  |
  r4 r fad\p |
  si4. dod8 re dod |
  si8[( lad]) si4 r |
  dod4 fad,8. la!16 si8. dod16 | \break

  si4 mi,8. si'16 mi8. si16 |
  lad16[( sol)] fad8 r4 r8 dod' \pp |
  re4. re8 re re |
  dod4.( si16[ la sol fad la dod] | \break

  si4. la16[ sol fad mi sol si] |
  la4. sol16[ fad mi re fad la] |
  sol4. fad16[ mi re dod mi sol] |
  fad4.) la8\cresc fad do'! |
  si4.( sol8[ si mi] | \break

  dod!4. la8[ dod mi] |
  re4) re, r8 re' \mf  |
  mi8 si dod2 |
  re2\trill r4 |
  r4 r r8 la\p | \break

  si4. do!8 si la |
  sol8[( fad)] mi4 r |
  fad4 fad8 fad si fad |
  mi4. la8 do! mi, |
  mi8 red r4 r8 lad'8 | \break

  sol4. dod8 mi sol, |
  sol4( fad) r |
  si8 re4 si sol8(~ |
  sol8 mi'4 sol, fad8 ~ |
  fad8 re'4 fad, mi8 ~ | \break

  mi8 dod'4 mi, dod8 |
  re4. si'8\cresc re fad |
  sol,4. mi8 sol mi' |
  lad,4) fad r8 mi'\mf |
  re8 si dod2\trill | \break

  si2 r4 |
  R2.*3 |
  r4 \fermata r r8 si\mf | \break

  dod4. re8 dod si |
  la8[( sold)] fad4 r |
  re'4 sold,8 sold dod dod |
  dod4 fad,8 fad si si | \break

  si4 mid, r8 si'\p |
  la4. sold8 fad re' |
  re4( ~ re16[ dod si la] sold8[ dod] |
  dod4 ~ dod16[ si la sold] fad8[ si] | \break

  si4 ~ si16[ la sol fad] mid8[ si'] |
  la4) fad r8 la |
  dod8 fad, sold2\trill |
  fad2.\fermata |
  \bar "|."
}

SopranoLyrics = \lyricmode {
     Ve -- drò  con mio di -- let -- to l'al -- ma del -- l'al -- ma mi -- a, del -- l'al -- ma mi -- a,
     il co -- re del mio cor,
     pien di con -- ten -- to,
     pien di con -- ten -- to.

     Ve -- drò  con mio di -- let -- to l'al -- ma del -- l'al -- ma mia, del -- l'al -- ma mi -- a,
     il cor di que -- sto cor
     pien di con -- ten -- to,
     pien di con -- ten -- to.

     E se dal car -- "o og" get -- to
     lun -- gi con -- vien che si -- a,
     con -- vien che si -- a,
     so -- spi -- re -- rò pe -- nan -- do
     o -- gui mo -- men -- to.
}

SopranoPart = \new Staff \Soprano {
  \autoBeamOff
  \clef "treble"
  \Common
  \SopranoVoice
} \addlyrics { \SopranoLyrics }

PianoPart = \new PianoStaff \Piano <<
  \new Staff = "RH" {
    \clef "treble"
    \Common
    \PianoRight
  }
  \new Staff = "LH" \with {
    \consists "Merge_rests_engraver"
    }{
    \clef "bass"
    \Common
    \mergeDifferentlyHeadedOn
    \mergeDifferentlyDottedOn

    <<
      \new Voice = "1" {
        \voiceOne
       \PianoLeftUpper
      }
      \new Voice = "2" {
        \voiceTwo
        \PianoLeftLower
      }
    >>
  }
>>
