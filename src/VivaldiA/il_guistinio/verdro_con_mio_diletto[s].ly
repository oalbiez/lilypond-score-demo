\version "2.18.2"
#(ly:set-option 'relative-includes #t)

\include "verdro_con_mio_diletto-notes.ily"


\score {
  % \transpose re do
    <<
      \SopranoPart
      \PianoPart
    >>
}
