\version "2.18.2"
#(ly:set-option 'relative-includes #t)

\include "../../includes/choir.ily"
\include "../../includes/instruments.ily"
\include "../../includes/paper.ily"
\include "amarilli_mia_bella/continuo.ily"
\include "amarilli_mia_bella/title.ily"
\include "amarilli_mia_bella/voice.ily"
\include "composer.ily"

\score {
    \new StaffGroup <<
      \new Staff \Soprano { \VoicePart }
      \new Staff \BassoContinuo { \ContinuoPart }
    >>
}
