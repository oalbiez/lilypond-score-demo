\include "italiano.ly"
\include "./definitions.ily"


VoiceLyrics = \lyricmode {
  A -- ma -- ril -- li, mia bel -- la
  Non cre -- di~o del mio cor
  Dol -- ce de -- si -- o
  D'es -- ser tu __ l'a -- mor mi -- o?

  Cre -- di -- lo pur
  E se ti -- mor t'as -- sa -- le
  Du -- bi -- tar non ti va -- le

  A -- pri -- mi~il pe -- to
  E ve -- drai scrit -- to~in co -- re
  A -- ma -- ril -- li A -- ma -- ril -- li A -- ma -- ril -- li
  è~il mio~a -- mo -- re

  Cre -- di -- lo pur
  E se ti -- mor t'as -- sa -- le
  Du -- bi -- tar non ti va -- le

  A -- pri -- mi~il pe -- to
  E ve -- drai scrit -- to~in co -- re
  A -- ma -- ril -- li A -- ma -- ril -- li A -- ma -- ril -- li
  è~il mio~a -- mo -- re
  A -- ma -- ril -- li è~il mio~a -- mo -- re.
}


PiuCresc = \markup{\italic{più cresc.}}
PocoRit = \markup{\italic{poco rit.}}
ATempo = \markup{\italic{a tempo}}
pSmorz = \markup{\dynamic p \italic{smorz.}}
Rit = \markup{\italic{rit.}}


VoiceMelody = \relative do'' {
  \autoBeamOff
  \clef "treble"
  \Common

  re2. \p la4 | % 1
  sib2 fad4 sol |
  la2 la4 \breathe re4\( |
  do4 la do4.-> sib8 | \break

  la2\) \breathe re2->~ | % 5
  re4 do8 sib do2 |
  sib2 \breathe re2->~\( |
  re4 do sib2~ | \break

  sib4\) \breathe la8\> la la2\! | % 9
  sol1 |
  sib2.-> \mf la8\> sol\! |
  la2 r8 fad\< fad sol | \break

  la4 fad \afterGrace sol2( la16)\!\> | % 13
  la1 |
  sib4.\! sol8 la4 sol8 fad |
  \afterGrace sol2(\< la16)\! la2 \breathe | \break

  re2.->\f do8 sib | % 17
  la2\> la8 \breathe fad fad sol\! |
  la4 \p fa! mi2 |
  re2 r4 fad8\( fad | \break

  sol2.( fad16[ sol la8]) | %21
  fad2\) r4 la8\< la |
  si4( do2 \grace{ sib16[ do] } sib16[ la sib8])\! |
  do2_\PiuCresc r4 dod8 dod | \break

  re2->\f re4 \breathe sib8 sol | % 25
  \afterGrace la1_\PocoRit( sol16) |
  sol1_\ATempo |
  sib2.->\mf la8 sol | \break

  la2 r8 fad\< fad sol | %29
  la4 fad \afterGrace sol2( la16)\! |
  la1 |
  sib4. sol8 la4 sol8 fad | \break

  sol2\< \grace la8 la2\! | % 33
  re2.\f-> do8 sib |
  la2 la8 \breathe fad fad_\pSmorz sol |
  la4 fa! mi2 | \break

  re2 r4 fad8\pp fad | % 37
  sol2.( fad16[ sol la8]) |
  fad2 r4 la8 la |
  si4(\< do2\! \grace{ sib16[ do] } sib16[ la sib8]) | \break

  do2 r4 dod8_\PiuCresc dod | % 41
  re2 re4 \breathe sib8 sol |
  \afterGrace la1_\Rit( sol16) |
  sol2 \breathe re'4\ppp\< re\! | \break

  mi!2->(~ mi8[ re16 do] si!8.[ do16]) | %45
  re2~ re8 r do8. si16 |
  si8.([\< do16 si8. do16] re16[ si do\!\> la] \grace do\( si\)[ la si sol]\! |
  la2~ la4.._\Rit sol16) |
  sol1\fermata |
  \bar "|."
}


VoicePart = \new Voice { \VoiceMelody } \addlyrics { \VoiceLyrics }
