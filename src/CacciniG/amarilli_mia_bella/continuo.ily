\include "italiano.ly"
\include "./definitions.ily"


aB = _\markup{\flat}
aD = _\markup{\sharp}
aThree = _\markup{3}
aFour = _\markup{4}
aFive = _\markup{5}
aSix = _\markup{6}
aSixD = _\markup{\sharp 6}
aSeven = _\markup{7}
aTenD = _\markup{\sharp 10}
aEleven = _\markup{11}
aFourteen = _\markup{14}


ContinuoMelody = \relative do' {
  \clef "bass"
  \Common

  sol2 fad\aSix | % 1
  sol2 re4 mib\aSix |
  re2\aEleven re4\aTenD re |
  fa4. fa8 fa4 sol |

  re4 do sib2 | %5
  mib4 re\aSix re~\aFour re\aTenD |
  sib2 sib'4 fad |
  sol2 re2 |

  mib4 do8~\aSix do8\aFive re4~\aEleven re8~\aTenD re8\aFourteen | %9
  sol,1 |
  sol'2 mib4\aSeven( mib\aSix) |
  re2\aEleven re,\aTenD |

  re'2 mi4\aSeven~ mi\aSix | % 13
  re2 re, |
  sol4 sol' re2 |
  sol,4 sol' re2 |

  sib2 sib4 do8\aSix~ do8\aFive | % 17
  re2 re, |
  re'2 la4\aEleven ~ la8\aTenD ~ la\aFourteen |
  re,2 re' |

  sol2 sol, | % 21
  re2 re' |
  sol2 sol, |
  do2 la |

  fad2 sol | % 25
  re'2\aEleven ~ re4\aTenD ~ re\aFourteen |
  sol,1\aD |
  sol'2 mib4\aSeven ~ mib\aSixD |

  re2 re, | %29
  re'2 mi4\aSeven~mi4\aSix |
  re1 |
  sol2 re |

  sol,2 re' | % 33
  sib2 sib4 do8\aSix ~ do8\aFive |
  re2 re, |
  re'2\aB la4\aEleven ~ la8\aTenD ~ la8\aFourteen |

  re,2 re' | % 37
  sol2 sol, |
  re'2 re |
  sol2 sol, |

  do2 la | % 41
  fad2 sol |
  re'2 ~ re4 ~ re4 |
  sol,2 sol |

  do4 re mi2 | % 45
  si2 fad |
  sol2 sol' |
  re2 ~ re4 ~ re4 |
  sol1\fermata
  \bar "|."
}

ContinuoPart = \new Voice { \ContinuoMelody }
