
Soprano = \with {
  instrumentName = "Soprano"
  shortInstrumentName = "S"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}

Alto = \with {
  instrumentName = "Alto"
  shortInstrumentName = "A"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}

Contralto = \with {
  instrumentName = "Contralto"
  shortInstrumentName = "C"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}

Tenor = \with {
  instrumentName = "Tenor"
  shortInstrumentName = "T"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}

CounterTenor = \with {
  instrumentName = \markup {
    \column { "Counter"
      \line { "Tenor" }
    }
  }
  shortInstrumentName = "Ct"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}

Baritone = \with {
  instrumentName = "Baritone"
  shortInstrumentName = "Ba"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}

Bass = \with {
  instrumentName = "Basse"
  shortInstrumentName = "B"
  midiInstrument = "choir aahs"
  \consists "Ambitus_engraver"
}
