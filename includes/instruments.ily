Piano = \with {
  instrumentName = "Piano"
  shortInstrumentName = "P"
  midiInstrument = "acoustic grand"
}

BassoContinuo = \with {
  instrumentName = \markup {
    \column { "Basso"
      \line { "continuo" }
    }
  }
  shortInstrumentName = "BC"
  midiInstrument = "grand piano"
}

Violin = \with {
  instrumentName = "Violon"
  shortInstrumentName = "Vl."
  midiInstrument = "string ensemble 1"
}

Cello = \with {
  instrumentName = "Violoncelle"
  shortInstrumentName = "Vlc."
  midiInstrument = "string ensemble 2"
}

Guitar = \with {
  instrumentName = "Guitar"
  shortInstrumentName = "G."
  midiInstrument = "Acoustic Guitar (steel)"
}
