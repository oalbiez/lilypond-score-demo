\paper {
  #(set-paper-size "a4")
  page-breaking = #ly:minimal-breaking
  oddFooterMarkup = \markup {
    \teeny
    \fill-line {
      \fromproperty #'header:title
      \concat{ "v" \fromproperty #'header:version }
       "CC BY 4.0"
    }
  }
  %annotate-spacing = ##t
}
