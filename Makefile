SOURCES = $(shell find src -name "*.ly")
SCORES = $(patsubst src/%.ly, pdf/%.pdf, $(SOURCES))


all: $(SCORES)

clean:
	@rm -rf pdf

pdf/%.pdf: src/%.ly
	lilypond -dno-point-and-click -o score -s "$<"
	@mkdir -p `dirname "$@"`
	@mv score.pdf "$@"
