[![pipeline status](https://gitlab.com/oalbiez/lilypond-score-demo/badges/master/pipeline.svg)](https://gitlab.com/oalbiez/lilypond-score-demo/commits/master)

# Partitions

Toutes les partitions ici sont sous license [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).


[Toutes les partitions](https://gitlab.com/oalbiez/lilypond-score-demo/builds/artifacts/master/browse/pdf/?job=lilypond)


## Amarilli mia Bella, Guilio Caccini (1546 – 1614)

- [Soprane](https://gitlab.com/oalbiez/lilypond-score-demo/builds/artifacts/master/file/pdf/CacciniG/amarilli_mia_bella[s].pdf?job=lilypond)
- [Contre ténor](https://gitlab.com/oalbiez/lilypond-score-demo/builds/artifacts/master/file/pdf/CacciniG/amarilli_mia_bella[Ct].pdf?job=lilypond)


## Vedro con mio diletto, Antonio Vivaldi (1678 – 1741)

- [Soprane](https://gitlab.com/oalbiez/lilypond-score-demo/builds/artifacts/master/file/pdf/VivaldiA/il_guistinio/verdro_con_mio_diletto[s].pdf?job=lilypond)
- [Contre ténor](https://gitlab.com/oalbiez/lilypond-score-demo/builds/artifacts/master/file/pdf/VivaldiA/il_guistinio/verdro_con_mio_diletto[Ct].pdf?job=lilypond)
